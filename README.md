**Repositorio academia capturetheatlas**

Este repositorio solo contiene los archivos theme, como la edición custom de los plugins necesarios bajo la carpeta theme

---

## Normas de nuestro Bitbucket

Para poder trabajar todos en sintonía.

1. **Clona** el repositorio en local.
2. **Crea una rama** siempre y cuando el cambio sea contundente.
3. Realiza los cambios y súbelos al entorno de pre.
4. Si todo está correcto, realiza **merge** con la rama principal.

---
