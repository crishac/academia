<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package thim
 */
get_header();
/**
 * thim_wrapper_loop_start hook
 *
 * @hooked thim_wrapper_loop_end - 1
 * @hooked thim_wapper_page_title - 5
 * @hooked thim_wrapper_loop_start - 30
 */

do_action( 'thim_wrapper_loop_start' );
?>
    <section class="error-404 not-found">
        <div class="page-404-content">
            <div class="row">
                <div class="col-xs-6">
					<?php
					if ( get_theme_mod( 'thim_single_404_left' ) ) {
						echo '<img src="' . get_theme_mod( 'thim_single_404_left' ) . '" alt="404-page" />';
					} else {
						echo '<img src="' . esc_url( get_template_directory_uri() . '-child/images/img404.jpg' ) . '" alt="404-page" />';
					}
					?>
                </div>
                <div class="col-xs-6 text-left">
                  <h2><?php echo _e( '404 ', 'eduma' ); ?><span
                              class="thim-color"><?php echo _e( 'Error!', 'eduma-child' ); ?></span></h2>
                  <p><?php echo _e( 'THE PAGE YOU ARE LOOKING FOR HAS GONE ON A TRIP ', 'eduma-child' ); ?> <br>
                      <?php echo _e( 'We can take you to the home page by click  ', 'eduma-child' ); ?>
                      <a href="<?php echo esc_url( get_home_url() ); ?>"
                         class="link_index"><strong> <?php echo _e( 'here.', 'eduma-child' ); ?> </strong></a></p>
                </div>
            </div>
        </div>
        <!-- .page-content -->
    </section>
<?php
/**
 * thim_wrapper_loop_end hook
 *
 * @hooked thim_wrapper_loop_end - 10
 * @hooked thim_wrapper_div_close - 30
 */
do_action( 'thim_wrapper_loop_end' );

get_footer(); ?>
