<?php


// Obtener idioma Actual
//$idioma = pll_current_language();
$idioma = 'en';

/* NECESARIO PARA IMPRESIÓN DE % DEL CURSO */
/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

$course = LP_Global::course();
$user   = LP_Global::user();

if ( ! $course || ! $user ) {
	return;
}

$course_id       = get_the_ID();
$course_rate_res = learn_press_get_course_rate( $course_id, false );
$course_rate     = $course_rate_res['rated'];
$total           = $course_rate_res['total'];
$price = $course->get_price_html();

// Obtener la información del producto del curso
$info_course = $course->get_post($id);
$name_course = $info_course->post_title;

$args = array(
				'post_type'         	=> 'product',
				's'										=> $name_course
		);

$query = new WP_Query($args);

if($query->have_posts()){
	while($query->have_posts()):$query->the_post();
		$course_product = new WC_Product(get_the_ID());
		$course_id = get_the_ID();
		$course_product_price = $course_product->get_price_html();
		$course_product_display = $course_product->get_price();
	endwhile; wp_reset_postdata();
}
$course_product_price_array = price_array($course_product_price);
$count_prices = count($course_product_price_array);

$price_course_old = false;
if ($count_prices == 1){
  $price_course = $course_product_price_array[0];
}
if($count_prices == 2){
  $price_course_old = $course_product_price_array[0];
  $price_course = $course_product_price_array[1];
}

/**
* Función mediante la que se imprimen los demás cursos que tiene el usuario
* @param $idioma
* @param $user
* @param $course_id
* @return
*/
function ver_cursos_usuario($idioma, $user, $course_id){
  // obtener los cursos del usuario
  $cursos_de_usuario = $user->get_orders();
  $cursos_de_usuario_new = array_diff($cursos_de_usuario, $course_id);

  $args = array(
            'post_type'         => 'lp_course',
            'order'             => 'ASC',
            'orderby'           => 'ID',
            'posts__in'         => $cursos_de_usuario
        );

  $query = new WP_Query($args);

  if($query->have_posts()){
		?>
			<div class="listado_mas_cursos_barra">
				<div class="titulo_mas_cursos_barra">
					<!-- Tus cursos (pll) -->
					<?php _e('Your courses','eduma-child'); ?>
				</div>
				<?php
		    while($query->have_posts()):$query->the_post();
		      if ($course_id != get_the_ID()){
		        ?>
		          <div class="contenido_mas_cursos_barra">
		            <a href="<?= the_permalink() ?>" target="_blank" class="mas_cursos_link"> <?= the_title().'<br>'; ?> </a>
		          </div>
		        <?php
		      }
		    endwhile;wp_reset_postdata();
				?>
			</div>
		<?php
  }
}

/* SECCIÓN USUARIO LOGUEADO PERO CURSO NO COMPRADO */
if ( ! $user->has_enrolled_course( $course->get_id() ) ) {
  /* COMPROBACIÓN USUARIO LOGUEADO */
  if(is_user_logged_in()){
    // Usuario logueado curso de pago (no gratuito)
    if ( !$course->is_free() ){
      ?>
      <!-- SECCIÓN DEL PRECIO DEL CURSO -->
			<div class="course-price precio_curso_barra_lateral">
					<div class="value <?php echo $class;?>">
							<?php if ( $price_course_old ) { ?>
									<span class="course-origin-price old_price_course_barra"><?= $price_course_old ?></span>
							<?php } ?>
							<div class="precio_curso_barra">
								<?php
								echo $price_course;
								?>
							</div>
					</div>
			</div>
      <!-- SECCIÓN DEL BOTÓN COMPRAR EL CURSO -->
			<div class="boton_comprar_barra">
				<div class="lp-button button button-purchase-course thim-enroll-course-button barra_comprar">
						<a href="?add-to-cart=<?= $course_id ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart boton_comprar_curso_barra" data-product_id="<?= $course_id ?>" data-product_sku="" aria-label="Agregar “<?= $name_course ?>” al carrito" rel="nofollow"><?= __( 'Buy this course', 'eduma' ) ?></a>
				</div>
			</div>
      <!-- SECCIÓN VISUALIZACIÓN DE VALORACIONES -->
      <div class="valoraciones_curso_barra">
        <div class="rating-box">
          <div class="review-star">
            <?php thim_print_rating( $course_rate ); ?> (<?php echo ( $course_rate ) ? esc_html( round( $course_rate, 1 ) ) : 0; ?>)
          </div>
        </div>
      </div>
      <!-- SECCIÓN VER PUNTOS DESTACADOS DEL CURSO CUSTOM FIELD TEXTEAREA -->
      <div class="destacados_curso_barra">
        <?php
        $destacados = get_post_meta(get_the_ID(), 'destacados', true);
        $destacados_part = explode('-', $destacados);
        $count = count($destacados_part);

        for ($i = 1; $i < $count; $i++){
          ?>
          <div class="punto_destacado">
            <?= $destacados_part[$i]; ?>
          </div>
          <?php
        }
        ?>
      </div>
      <!-- SECCIÓN MÁS CURSOS DEL USUARIO -->
      <div class="otros_cursos">
        <?php ver_cursos_usuario($idioma, $user, $course_id); ?>
      </div>
      <?php
    }
    // Usuario logueado curso gratuito (si gratuito)
    if ( $course->is_free() ){
      ?>
        <div class="course-price precio_free_barra_lateral">
          <div class="value curso_free_barra">
            <!-- curso free (pll) -->
            <?php _e('Free','eduma-child'); ?>
          </div>
        </div>
        <!-- SECCIÓN DEL BOTÓN TOMAR ESTE CURSO GRATUITO -->
        <form name="enroll-course" class="enroll-course form-purchase-course" method="post" enctype="multipart/form-data">
           <?php do_action( 'learn-press/before-enroll-button' ); ?>
           <input type="hidden" name="enroll-course" value="<?php echo esc_attr( $course->get_id() ); ?>"/>
           <input type="hidden" name="enroll-course-nonce" value="<?php echo esc_attr( LP_Nonce_Helper::create_course( 'enroll' ) ); ?>"/>
           <button class="lp-button button button-enroll-course barra_comprar">
              <?php echo esc_html( apply_filters( 'learn-press/enroll-course-button-text', __( 'Take this course', 'eduma' ) ) ); ?>
           </button>
          <input type="hidden" name="redirect_to" value="<?php echo esc_url( $login_redirect ); ?>">
          <?php do_action( 'learn-press/after-enroll-button' ); ?>
        </form>
        <!-- SECCIÓN VISUALIZACIÓN DE VALORACIONES -->
        <div class="valoraciones_curso_barra">
          <div class="rating-box">
            <div class="review-star">
              <?php thim_print_rating( $course_rate ); ?> (<?php echo ( $course_rate ) ? esc_html( round( $course_rate, 1 ) ) : 0; ?>)
            </div>
          </div>
        </div>
        <!-- SECCIÓN VER PUNTOS DESTACADOS DEL CURSO CUSTOM FIELD TEXTEAREA -->
        <div class="destacados_curso_barra">
          <?php
          $destacados = get_post_meta(get_the_ID(), 'destacados', true);
          $destacados_part = explode('-', $destacados);
          $count = count($destacados_part);

          for ($i = 1; $i < $count; $i++){
            ?>
            <div class="punto_destacado">
              <?= $destacados_part[$i]; ?>
            </div>
            <?php
          }
          ?>
        </div>
        <!-- SECCIÓN MÁS CURSOS DEL USUARIO -->
        <div class="otros_cursos">
          <?php ver_cursos_usuario($idioma, $user, $course_id); ?>
        </div>
      <?php
    }
    // Visualización de otros cursos comprados por el usuario

    return;
  } // if curso de pago
}

$course_data       = $user->get_course_data( $course->get_id() );
$course_results    = $course_data->get_results( false );
$passing_condition = $course->get_passing_condition();
//$url = $_SERVER[‘REQUEST_URI’];
$current = 0;

if ( isset( $course_results['result'] ) ) {
	$current = round( $course_results['result'], 2 );
}

$passed = $current >= $passing_condition;

// Comprobación de que el usuario está logueado
if(is_user_logged_in()){
  ?>
    <!-- SECCIÓN PROGRESO -->
    <div class="tu_progreso_barra">
      <!-- progreso (pll) -->
      <?php _e('Progress','eduma-child'); ?>
    </div>
    <div class="porcentaje_completado_barra">
      <div class="lp-course-progress<?php echo $passed ? ' passed' : ''; ?>" data-value="<?php echo $current; ?>"
         data-passing-condition="<?php echo $passing_condition; ?>">
         <!-- Modificaciones Bitesse -->
        <?php if ( false !== ( $heading = apply_filters( 'learn-press/course/result-heading', __( 'Complete', 'eduma' ) ) ) ) { ?>
          <?php
          //$idioma = pll_current_language();
          $idioma = 'es';
          ?>
          <label class="lp-course-progress-heading testv3-child contenido_porcentaje">
            <span class="value result">
              <b class="number"><?php echo $current; ?></b>%
            </span>
            &nbsp;
            <?php echo $heading; ?>
          </label>
        <?php } ?>

        <div class="lp-progress-bar value">
          <div class="lp-progress-value percentage-sign"
             style="width: <?php echo $current; ?>%;">
          </div>
        </div>
      </div>
    </div>
    <?php
    if ($user){
      $url = $_SERVER["REQUEST_URI"];
      $swbutton = 0;
      $url_array = explode('/', $url);
      $count_url = count($url_array);
      $url_array = explode('/', $url);
      if ($url_array[$count_url-2] != 'cursos' && $url_array[$count_url-2] != 'courses'){
        $swbutton = 1;
      }

      if ($swbutton == 1){
        ?>
          <div class="boton_continuar_barra">
            <form name="continue-course" class="continue-course form-button lp-form" method="post" action="<?php echo $user->get_current_item( get_the_ID(), true ); ?>">
                <button type="submit" class="lp-button button"><?php _e( 'Continue', 'learnpress' ); ?></button>
            </form>
          </div>
        <?php
      }
    }
    ?>


		<?php // NOTE: Reviews ?>

		<!-- SECCIÓN VISUALIZACIÓN DE VALORACIONES -->
		<div class="valoraciones_curso_barra">
			<div class="rating-box">
				<div class="review-star">
					<?php thim_print_rating( $course_rate ); ?> (<?php echo ( $course_rate ) ? esc_html( round( $course_rate, 1 ) ) : 0; ?>)
				</div>
			</div>
		</div>
		<a class="review-barra" href="#tab-reviews" data-toggle="tab">Deja tu review</a>








    <!-- SECCIÓN MÁS CURSOS DEL USUARIO -->
    <div class="otros_cursos">
      <?php ver_cursos_usuario($idioma, $user, $course_id); ?>
    </div>

		<?php // NOTE: viajes relacionados ?>
<?php
$enlaceviajes = get_field( "enlaces_fotograficos" );
 ?>
 <?php if (!empty($enlaceviajes)){
?>
<div class="viajesrelacionados">
	<div class="titulo_mas_cursos_barra">
		<?php _e('PRACTICE WITH US!','eduma-child'); ?>
	</div>

	<?php
	$enlaceviajes = get_field( "enlaces_fotograficos" );

	echo $enlaceviajes;

	 ?>
</div>
<?php
 } ?>






    <?php
}// If usuario login








?>
