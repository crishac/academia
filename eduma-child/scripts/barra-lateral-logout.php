<?php
// Obtener idioma Actual
//$idioma = pll_current_language();
//$idioma = 'es';

//función precio
function price_array($price){
  //echo 'LLEGAMOS A FUNCIÓN price_array';
    $price = str_replace(',', '.', $price);
    $price = strip_tags($price);
    $delstring = array('"');
    $price = str_replace($del, '', $price);
    $price_array = explode(' ', $price);
    // echo '<pre>';
    // var_dump($price_array);
    // echo '</pre>';
    return $price_array;
}

// Comprobación de que el usuario no está logueado
if(!is_user_logged_in()){
  $user   = LP_Global::user();
  $course = LP_Global::course();
  if ( ! $course || ! $user ) {
  	return;
  }

  $class = '';
  $class .= ( $course->has_sale_price() ) ? ' has-origin' : '';
  if ( $course->is_free() ) {
      $class .= ' free-course';
  }

  if ( ! $price = $course->get_price_html() ) {
  	return;
  }

  // Obtener la información del producto del curso
  $info_course = $course->get_post($id);
  $name_course = $info_course->post_title;

  $args = array(
          'post_type'         	=> 'product',
          's'										=> $name_course
      );

  $query = new WP_Query($args);

  if($query->have_posts()){
  	while($query->have_posts()):$query->the_post();
  		$course_product = new WC_Product(get_the_ID());
      $course_id = get_the_ID();
  		$course_product_price = $course_product->get_price_html();
      $course_product_display = $course_product->get_price();
  	endwhile; wp_reset_postdata();
  }
$course_product_price_array = price_array($course_product_price);
$count_prices = count($course_product_price_array);

$price_course_old = false;
if ($count_prices == 1){
  $price_course = $course_product_price_array[0];
}
if($count_prices == 2){
  $price_course_old = $course_product_price_array[0];
  $price_course = $course_product_price_array[1];
}
$currency_symbol = get_woocommerce_currency_symbol();

    //if ( $idioma == 'es'){
       if ( !$course->is_free() ){
          ?>
          <!-- SECCIÓN DEL PRECIO DEL CURSO -->
          <div class="course-price precio_curso_barra_lateral">
              <div class="value <?php echo $class;?>">
                  <?php if ( $price_course_old ) { ?>
                      <span class="course-origin-price old_price_course_barra"><?= $price_course_old ?></span>
                  <?php } ?>
                  <div class="precio_curso_barra">
                    <?php
                    echo $price_course;
                    ?>
                  </div>
              </div>
          </div>
          <div class="boton_comprar_barra">
            <div class="lp-button button button-purchase-course thim-enroll-course-button barra_comprar">
                <a href="?add-to-cart=<?= $course_id ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart boton_comprar_curso_barra" data-product_id="<?= $course_id ?>" data-product_sku="" aria-label="Agregar “<?= $name_course ?>” al carrito" rel="nofollow"><?= __( 'Buy this course', 'eduma' ) ?></a>
            </div>
          </div>
          <?php
       }else{
         ?>
         <div class="course-price precio_curso_barra_lateral">
           <div class="value curso_free_barra">
             <!-- curso free (pll) -->
             <?php echo _e( 'Free', 'eduma-child' ) ?>
           </div>
         </div>
         <!-- SECCIÓN DEL BOTÓN TOMAR ESTE CURSO GRATUITO -->
         <form name="enroll-course" class="enroll-course form-purchase-course" method="post" enctype="multipart/form-data">
            <?php do_action( 'learn-press/before-enroll-button' ); ?>
            <input type="hidden" name="enroll-course" value="<?php echo esc_attr( $course->get_id() ); ?>"/>
            <input type="hidden" name="enroll-course-nonce" value="<?php echo esc_attr( LP_Nonce_Helper::create_course( 'enroll' ) ); ?>"/>
            <button class="lp-button button button-enroll-course">
               <?php echo esc_html( apply_filters( 'learn-press/enroll-course-button-text', __( 'Take this course', 'eduma' ) ) ); ?>
            </button>
           <input type="hidden" name="redirect_to" value="<?php echo esc_url( $login_redirect ); ?>">
           <?php do_action( 'learn-press/after-enroll-button' ); ?>
         </form>
         <?php
       }
      ?>
      <!-- SECCIÓN VISUALIZACIÓN DE VALORACIONES -->
      <div class="valoraciones_curso_barra">
        <div class="rating-box">
    			<div class="review-star">
    				<?php thim_print_rating( $course_rate ); ?> (<?php echo ( $course_rate ) ? esc_html( round( $course_rate, 1 ) ) : 0; ?>)
    			</div>
    		</div>
      </div>

      <!-- SECCIÓN VER PUNTOS DESTACADOS DEL CURSO CUSTOM FIELD TEXTEAREA -->
      <div class="destacados_curso_barra">
        <?php
        $destacados = get_post_meta(get_the_ID(), 'destacados', true);
        $destacados_part = explode('-', $destacados);
        $count = count($destacados_part);

        for ($i = 1; $i < $count; $i++){
          ?>
          <div class="punto_destacado">
            <?= $destacados_part[$i]; ?>
          </div>
          <?php
        }
        ?>
      </div>

      <!-- SECCIÓN BOTÓN DE ACCESO SI ERES ALUMNO -->
      <div class="boton_acceso_barra">
        <a href="/cuenta/"><?php echo _e( 'Already a student?', 'eduma-child' ) ?><b> <?php echo _e( 'Login', 'eduma-child' ) ?></b></a>
      </div>
    <?php
  //} // IF del idioma ES

/*
  if ( $idioma == 'en'){
    if ( !$course->is_free() ){
      ?>
      <!-- SECCIÓN DEL PRECIO DEL CURSO INGLÉS -->
      <div class="course-price precio_curso_barra_lateral">
          <div class="value <?php echo $class;?>">
              <?php if ( $course->has_sale_price() ) { ?>
                  <!-- <span class="course-origin-price"> <?php echo $course->get_origin_price_html(); ?></span> -->
              <?php } ?>
              <div class="precio_curso_barra">
                <?php echo $price; ?>
              </div>
          </div>
      </div>
      <!-- SECCIÓN DEL BOTÓN COMPRAR EL CURSO INGLÉS -->
      <div class="boton_comprar_barra">
        <form name="purchase-course" class="purchase-course form-purchase-course <?php echo $guest_checkout; ?>" method="post" enctype="multipart/form-data">
          <?php do_action( 'learn-press/before-purchase-button' ); ?>
          <input type="hidden" name="purchase-course" value="<?php echo esc_attr( $course->get_id() ); ?>"/>
          <input type="hidden" name="purchase-course-nonce" value="<?php echo esc_attr( LP_Nonce_Helper::create_course( 'purchase' ) ); ?>"/>
          <button class="lp-button button button-purchase-course thim-enroll-course-button barra_comprar">
            <?php echo esc_html( apply_filters( 'learn-press/purchase-course-button-text', __( 'Buy this course', 'eduma' ) ) ); ?>
          </button>
          <input type="hidden" name="redirect_to" value="<?php echo esc_url( $login_redirect ); ?>">
          <?php do_action( 'learn-press/after-purchase-button' ); ?>
        </form>
      </div>
      <?php
    }else{
      ?>
      <div class="course-price precio_curso_barra_lateral">
        <div class="value curso_free_barra">
          <!-- curso free (pll) -->
          <?php _e('Free','eduma-child'); ?>
        </div>
      </div>
      <!-- SECCIÓN DEL BOTÓN TOMAR ESTE CURSO GRATUITO -->
      <form name="enroll-course" class="enroll-course form-purchase-course" method="post" enctype="multipart/form-data">
         <?php do_action( 'learn-press/before-enroll-button' ); ?>
         <input type="hidden" name="enroll-course" value="<?php echo esc_attr( $course->get_id() ); ?>"/>
         <input type="hidden" name="enroll-course-nonce" value="<?php echo esc_attr( LP_Nonce_Helper::create_course( 'enroll' ) ); ?>"/>
         <button class="lp-button button button-enroll-course barra_comprar">
            <?php echo esc_html( apply_filters( 'learn-press/enroll-course-button-text', __( 'Take this course', 'eduma' ) ) ); ?>
         </button>
        <input type="hidden" name="redirect_to" value="<?php echo esc_url( $login_redirect ); ?>">
        <?php do_action( 'learn-press/after-enroll-button' ); ?>
      </form>
      <?php
    }
    ?>

    <!-- SECCIÓN VISUALIZACIÓN DE VALORACIONES INGLÉS -->
    <div class="valoraciones_curso_barra">
      <div class="rating-box">
        <div class="review-star">
          <?php thim_print_rating( $course_rate ); ?> (<?php echo ( $course_rate ) ? esc_html( round( $course_rate, 1 ) ) : 0; ?>)
        </div>
      </div>
    </div>
    <!-- SECCIÓN VER PUNTOS DESTACADOS DEL CURSO CUSTOM FIELD TEXTEAREA -->
    <div class="destacados_curso_barra">
      <?php
      $destacados = get_post_meta(get_the_ID(), 'destacados', true);
      $destacados_part = explode('-', $destacados);
      $count = count($destacados_part);

      for ($i = 1; $i < $count; $i++){
        ?>
        <div class="punto_destacado">
          <?= $destacados_part[$i]; ?>
        </div>
        <?php
      }
      ?>
    </div>
    <!-- SECCIÓN BOTÓN DE ACCESO SI ERES ALUMNO -->
    <div class="boton_acceso_barra">
      <a href="/account/"><?php echo _e( 'Already a student?', 'eduma-child' ) ?><b> <?php echo _e( 'Loginn', 'eduma-child' ) ?></b></a>
    </div>
  <?php
  }// IF del idioma EN
  */
}
?>
