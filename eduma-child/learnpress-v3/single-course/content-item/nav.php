<?php
/**
 * Template for displaying next/prev item in course.
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 3.0.0
 */

defined( 'ABSPATH' ) or die();

if ( ! isset( $prev_item ) && ! isset( $next_item ) ) {
	return;
}
?>
<div class="course-item-nav">
	<?php if ( $prev_item ) { ?>
        <div class="course-content-lesson-nav course-item-prev">
            <span>
							<a href="<?php echo $prev_item->get_permalink(); ?>">
								<img src="/wp-content/themes/eduma-child/img/fle_izqxs.png">
								<?php _e( 'Prev', 'eduma' ); ?>
							</a>
						</span>
            <a href="<?php echo $prev_item->get_permalink(); ?>">
							<?php echo $prev_item->get_title(); ?>
            </a>
        </div>
	<?php } ?>

	<?php if ( $next_item ) { ?>
        <div class="course-content-lesson-nav course-item-next">
            <span>
							<a href="<?php echo $next_item->get_permalink(); ?>">
								<?php _e( 'Next', 'eduma' ); ?>
								<img src="/wp-content/themes/eduma-child/img/fle_derxs.png">
							</a>
						</span>
            <a href="<?php echo $next_item->get_permalink(); ?>">
							<?php echo $next_item->get_title(); ?>
            </a>
        </div>
	<?php } ?>
</div>
