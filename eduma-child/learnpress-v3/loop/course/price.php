<?php
/**
 * Template for displaying price of course within the loop.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/loop/course/price.php.
 *
 * @author  ThimPress
 * @package  Learnpress/Templates
 * @version  3.0.1
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

$course = LP_Global::course();

if ( ! $course ) {
	return;
}
$class = ( $course->has_sale_price() ) ? ' has-origin' : '';
if ( $course->is_free() ) {
	$class .= ' free-course';
}

// LOCALIZAR PRECIO DEL CURSO DE PRODUCTOS
$info_course = $course->get_post($id);
$name_course = $info_course->post_title;
// BUSCAR CUSTOM POST TYPE PRODUCTO SEGÚN EL NOMBRE
$args = array(
        'post_type'         	=> 'product',
        's'										=> $name_course
    );

$query = new WP_Query($args);

if($query->have_posts()){
	while($query->have_posts()):$query->the_post();
		//$course_price = get_post_meta( get_the_ID(), '_price', true );
		//$course_price = get_post_meta( get_the_ID(), '_regular_price', true );
		$course_product = new WC_Product(get_the_ID());
		$course_product_price = $course_product->get_price_html();
	endwhile; wp_reset_postdata();

}
//echo 'PRECIO: '.$course_product_price.'<br>';
?>

<div class="course-price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
	<?php if ( $price_html = $course->get_price_html() ) { ?>
        <div class="value <?php echo $class; ?> price_view TEST" itemprop="price">
			<?php if ( $course->get_origin_price() != $course->get_price() ) { ?>
				<?php $origin_price_html = $course->get_origin_price_html(); ?>
                <!-- <span class="course-origin-price"><php echo $origin_price_html; ?></span> -->
			<?php } ?>
			<?php
			//echo $price_html;
			echo $course_product_price;
			?>
        </div>
        <meta itemprop="priceCurrency" content="<?php echo learn_press_get_currency(); ?>"/>
	<?php } ?>
</div>
