<?php

function thim_child_enqueue_styles() {
	wp_enqueue_style( 'thim-parent-style', get_template_directory_uri() . '/style.css', array(), THIM_THEME_VERSION  );
}

add_action( 'wp_enqueue_scripts', 'thim_child_enqueue_styles', 1000 );

/**
* Incluir fuentes de google fonts
*/
function capture_enqueue_font_os() {
wp_enqueue_style('google-font-oswald', 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&family=Oswald:wght@300;400;700&display=swap', array());}
add_action( 'wp_enqueue_scripts', 'capture_enqueue_font_os' );

function capture_enqueue_font_sans() {
wp_enqueue_style('google-font-open-sans', 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&family=Oswald:wght@300;700&display=swap', array());}
add_action( 'wp_enqueue_scripts', 'capture_enqueue_font_sans' );

/**
* Función para incluir login/logout
*/
/*
add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2);
function add_loginout_link( $items, $args ) {

	if ($args->theme_location == 'primary') {
		if (is_user_logged_in())
		{
			$items .= '<li class="menu-item btn-menu btn-logout">
						<a href="'. wp_logout_url(get_permalink()) .'">'. _e("Logout") .'</a>
						</li>';
		}
		else
		{
			$items .= '<li class="menu-item btn-menu btn-login">
						<a href="'. wp_login_url(get_permalink()) .'">'. _e("Login") .'</a>
						</li>';
		}
	}
	return $items;
}
*/

/*
function wp_loginout( $redirect = '', $echo = true ) {
    if ( ! is_user_logged_in() ) {
        $link = '<a href="' . esc_url( wp_login_url( $redirect ) ) . '">' . __( 'Log in' ) . '</a>';
    } else {
        $link = '<a href="' . esc_url( wp_logout_url( $redirect ) ) . '">' . __( 'Log out' ) . '</a>';
    }

    if ( $echo ) {
        echo apply_filters( 'loginout', $link );
    } else {
        return apply_filters( 'loginout', $link );
    }
}
*/


add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );
function add_loginout_link( $items, $args ) {
		$redirect = home_url( add_query_arg( array() ) );
    if (is_user_logged_in() && $args->theme_location == 'primary') {
        //$items .= '<li class ="login"><a href="'. wp_logout_url() .'">'. _e( 'Logout', 'eduma-child' ) .'</a></li>';
				/*
				$items .= '<li class="menu-item btn-menu btn-account">
							<a href="/mi-cuenta/" class="tc-menu-inner">'. __( 'Account', 'eduma-child' ) .'</a>
							</li>';
				$items .= '<li class="menu-item btn-menu btn-logout">
							<a href="'. wp_logout_url(get_permalink()) .'" class="tc-menu-inner">'. __( 'Logout', 'eduma-child' ) .'</a>
							</li>';
				*/
				if (ICL_LANGUAGE_CODE == 'es'){
					$items .= '<li class="menu-item menu-item-type-custom tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default last-menu-item">
						<a href="/mi-cuenta" class="tc-menu-inner">'. __( 'Account', 'eduma-child' ) .'</a>
						</li>';
					$items .= '<li class="menu-item menu-item-type-custom tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default last-menu-item">
						<a href="'. wp_logout_url('/es') .'" class="tc-menu-inner">'. __( 'Logout', 'eduma-child' ) .'</a>
						</li>';
				}
				if (ICL_LANGUAGE_CODE == 'en'){
					$items .= '<li class="menu-item menu-item-type-custom tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default last-menu-item">
						<a href="/my-account" class="tc-menu-inner">'. __( 'Account', 'eduma-child' ) .'</a>
						</li>';
					$items .= '<li class="menu-item menu-item-type-custom tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default last-menu-item">
						<a href="'. wp_logout_url(get_permalink()) .'" class="tc-menu-inner">'. __( 'Logout', 'eduma-child' ) .'</a>
						</li>';
				}
				// wp_logout_url('/redirect/url/goes/here')
				//$item->url = $item->url . "&_wpnonce=" . wp_create_nonce( 'log-out' ).'&redirect_to='.wp_login_url();
				// https://academycapture.bitesse.es/wp-login.php?action=logout&redirect_to=https%3A%2F%2Facademycapture.bitesse.es%2F&_wpnonce=c178457059
    }elseif (!is_user_logged_in() && $args->theme_location == 'primary') {
        //$items .= '<li class ="login"><a href="'. site_url('wp-login.php') .'">'. _e( 'Login', 'eduma-child' ) .'</a></li>';

				if (ICL_LANGUAGE_CODE == 'es'){
					$items .= '<li class="menu-item menu-item-type-custom tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default last-menu-item">
						<a href="/es/cuenta" class="tc-menu-inner">'. __( 'Login', 'eduma-child' ) .'</a>
						</li>';
				}
				if (ICL_LANGUAGE_CODE == 'en'){
					$items .= '<li class="menu-item menu-item-type-custom tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default last-menu-item">
						<a href="/account?redirect='. $redirect .'" class="tc-menu-inner">'. __( 'Login', 'eduma-child' ) .'</a>
						</li>';
				}
    }
    return $items;
}


/**
* Redirecciónar después de login
*/

/* INTENTO 17/05/2021 */
/*
add_filter('wp_authenticate_user', 'my_auth_login',10,2);
function my_auth_login ($user, $password) {
	$role = $user->roles[0];
	$myaccount = $_SERVER['HTTP_REFERER'];
	if ( $role == 'customer' || $role == 'subscriber' ) {
		$myaccount = explode('=', $myaccount);
		//echo 'TEST LLEGA: ';
		//echo '<pre>';print_r($myaccount[1]);echo '</pre>';

		wp_redirect($myaccount[1]);
		return $user;
		exit();
	}

	echo '<pre>';print_r($role);echo '</pre>';
     //do any extra validation stuff here
		 echo '<pre>';print_r($password);echo '</pre>';

		//die('Call');

     return $user;
}
*/

/*
function redirect_after_login($user_login, $user) {
    $role = $user->roles[0];
		echo 'ROLE: ';
}
add_action('wp_login', 'redirect_after_login', 10, 2);
*/

/*
function wc_custom_user_redirect( $redirect, $user ) {
  // Get the first of all the roles assigned to the user
  $role = $user->roles[0];
  $dashboard = admin_url();
	echo 'ROL: '.$role;
  //$myaccount = get_permalink( wc_get_page_id( 'courses' ) );
	$myaccount = $_SERVER['HTTP_REFERER'];
  if( $role == 'administrator' ) {
    //Redirect administrators to the dashboard
    $redirect = $dashboard;
  } elseif ( $role == 'shop-manager' ) {
    //Redirect shop managers to the dashboard
    $redirect = $dashboard;
  } elseif ( $role == 'editor' ) {
    //Redirect editors to the dashboard
    $redirect = $dashboard;
  } elseif ( $role == 'author' ) {
    //Redirect authors to the dashboard
    $redirect = $dashboard;
  } elseif ( $role == 'customer' || $role == 'subscriber' ) {
		echo 'TEST PAGE';
    //Redirect customers and subscribers to the "My Account" page
    $redirect = $myaccount;
  } else {
    //Redirect any other role to the previous visited page or, if not available, to the home
    $redirect = wp_get_referer() ? wp_get_referer() : home_url();
  }
	echo 'FIN REDIRECT: '.$redirect;
	exit();
  //return $redirect;
}
add_filter( 'woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2 );
*/

// echo '<pre>';
// var_dump($_GET);
// echo '</pre>';
// echo '<pre>';
// var_dump($_POST);
// echo '</pre>';
// echo 'HTTP_REFERER';
// echo '<pre>';
// var_dump($_SERVER['HTTP_REFERER']);
// echo '</pre>';
/*
if ( (isset($_GET['action']) && $_GET['action'] != 'logout') || (isset($_POST['login_location']) && !empty($_POST['login_location'])) ) {
	// echo 'LLEGA A REDIRECT';
	// $location = $_SERVER['HTTP_REFERER'];
	// echo 'location: '.$location;
	// wp_safe_redirect($location);
	// exit();

	add_filter('login_redirect', 'my_login_redirect', 10, 3);
	function my_login_redirect() {
    $location = $_SERVER['HTTP_REFERER'];
    wp_safe_redirect($location);
    exit();
	}

}
*/
/**
*  Ocultar barra de admin a todos los que no sean admin
*/
if ( ! current_user_can( 'manage_options' ) ) {
add_filter('show_admin_bar', '__return_false');
}

/**
* Función para incluir archivo js personalizado
*/

add_action('wp_enqueue_scripts', 'myscript_enqueue_custom_js');
function myscript_enqueue_custom_js() {
    //wp_enqueue_script('custom', get_stylesheet_directory_uri().'/js/custom.js');
		wp_enqueue_script('mytheme-captue-js', get_stylesheet_directory_uri().'/js/custom.js', array('jquery'), false, true);
}

/**
* Custom Post Type FAQ
*/
// Register Custom Post Type
function custom_post_type_faq() {

	$labels = array(
		'name'                  => _x( 'faqs', 'Post Type General Name', 'captureacademy' ),
		'singular_name'         => _x( 'faq', 'Post Type Singular Name', 'captureacademy' ),
		'menu_name'             => __( 'FAQS', 'captureacademy' ),
		'name_admin_bar'        => __( 'FAQS', 'captureacademy' ),
		'archives'              => __( 'Archivo de FAQS', 'captureacademy' ),
		'attributes'            => __( '', 'captureacademy' ),
		'parent_item_colon'     => __( 'FAQ Padre', 'captureacademy' ),
		'all_items'             => __( 'Todos los FAQS', 'captureacademy' ),
		'add_new_item'          => __( 'Agregar nuevo FAQ', 'captureacademy' ),
		'add_new'               => __( 'Agregar nuevo FAQ', 'captureacademy' ),
		'new_item'              => __( 'Nuevo FAQ', 'captureacademy' ),
		'edit_item'             => __( 'Editar FAQ', 'captureacademy' ),
		'update_item'           => __( 'Actualizar FAQ', 'captureacademy' ),
		'view_item'             => __( 'Ver FAQ', 'captureacademy' ),
		'view_items'            => __( 'Ver FAQS', 'captureacademy' ),
		'search_items'          => __( 'Buscar FAQ', 'captureacademy' ),
		'not_found'             => __( 'No encontrado', 'captureacademy' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'captureacademy' ),
		'featured_image'        => __( 'Imagen destacada', 'captureacademy' ),
		'set_featured_image'    => __( 'Agregar imagen destacada', 'captureacademy' ),
		'remove_featured_image' => __( 'Eliminar imagen destacada', 'captureacademy' ),
		'use_featured_image'    => __( 'Usar imagen destacada', 'captureacademy' ),
		'insert_into_item'      => __( 'Insertar dentro de FAQ', 'captureacademy' ),
		'uploaded_to_this_item' => __( 'Actualizado este FAQ', 'captureacademy' ),
		'items_list'            => __( 'Lista de FAQS', 'captureacademy' ),
		'items_list_navigation' => __( 'Navegación de FAQS', 'captureacademy' ),
		'filter_items_list'     => __( 'Filtrar FAQS', 'captureacademy' ),
	);
	$args = array(
		'label'                 => __( 'faq', 'captureacademy' ),
		'description'           => __( 'faq de capture academy', 'captureacademy' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-lightbulb',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'faqs', $args );

}
add_action( 'init', 'custom_post_type_faq', 0 );

/*
* Habilita los shortcodes en el widget de Texto
*/
add_filter('widget_text','do_shortcode');

/**
* Prueba de funcionamiento de shortcode
*/
function codigoPHP1 (){
	include ( get_template_directory() ."-child/scripts/barra-lateral-logout.php");
}
add_shortcode ('logoutbar', 'codigoPHP1');
function codigoPHP2 (){
	include ( get_template_directory() ."-child/scripts/barra-lateral-login.php");
}
add_shortcode ('loginbar', 'codigoPHP2');

/**
* Metabox para los campos del FAQ
*/
function cta_meta_faq() {
	add_meta_box('cta_metaboxes_faq', 'Curso al que pertenece el FAQ', 'cta_diseno_metaboxes_faq', 'faqs', 'normal', 'high', null);
}
add_action('add_meta_boxes', 'cta_meta_faq');

function cta_diseno_metaboxes_faq($post) {
	$args = array(
			'post_type'					=> 'lp_course',
			'posts_per_page'		=> -1,
			'order'							=> 'DESC'
	);

	$query = new WP_Query($args);

	if($query->have_posts()){
		?>
		<div class="metabox-faq">
			<div class="form-group">
				<?php
				$curso_seleccionado = get_post_meta($post->ID, 'curso-metabox', true);
				?>
				<select name="curso-metabox">
					<option value="0">Seleccione un curso</option>
					<?php
						while($query->have_posts()):$query->the_post();
							$id_actual = get_the_ID();;
							?>
								<option value="<?php the_ID(); ?>" <?php if ($id_actual == $curso_seleccionado){ echo 'selected'; } ?> ><?php the_title(); ?></option>
							<?php
						endwhile;wp_reset_postdata();
					?>
				</select>
			</div>
		</div>
		<?php
	}
}

function cta_guardar_metabox_curso($post_id, $post, $update) {
	if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
		return $post_id;

	$metabox_curso_faq = '';

	if(isset($_POST['curso-metabox'])){
		$metabox_curso_faq = $_POST['curso-metabox'];
	}
	update_post_meta($post_id, 'curso-metabox', $metabox_curso_faq);
}
add_action('save_post', 'cta_guardar_metabox_curso', 10, 3);

/**
* Redireccionar link continue shopping (continuar comprando)
*/
add_filter( 'woocommerce_continue_shopping_redirect', 'bbloomer_change_continue_shopping' );

function bbloomer_change_continue_shopping() {
   return wc_get_page_permalink( 'courses' );
}

/**
* Redireccionar después de hacer login
*/
/*
function login_redirect() {
	global $user;
	//$request = $_SERVER["HTTP_REFERER"];
	$request = '/courses';
	$redirect_to = "/";
	if( isset( $user->roles ) && is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) {
		return '/wp-admin/';
	}else{
		return $request;
	}

}
add_action('wp_login', 'login_redirect');
*/
/*
add_filter( 'login_redirect', 'prefix_login_redirect', 10, 3 );
function prefix_login_redirect() {
		global $user;
		$request = $_SERVER["HTTP_REFERER"];
		$redirect_to = "/";
    //If we are viewing the account page, redirect to it after logging in
    if( strpos( $redirect_to, 'account' ) !== false ) {
        //If user is an administrator, redirect to WordPress dashboard
        if( isset( $user->roles ) && is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) {
            return '/wp-admin/';
        }
        //return esc_url( home_url( '/account/' ) );//Otherwise, redirect to account page
				return $request;
    }
    return $redirect_to;//Redirect to current page after logging in
}
*/

/*
add_filter('login_redirect', 'redirect_previous_page', 10, 1);
function redirect_previous_page( $redirect_to ){
    global $user;
    $request = $_SERVER["HTTP_REFERER"];
    if ( in_array( $user->roles[0], array( 'administrator') ) ) {
        return admin_url();
    } elseif ( in_array( $user->roles[0], array( 'subscriber') ) ) {
        return $request;
    }
    return $redirect_to;
}
*/

// ---------------------------------
// Redirección de registro (login)
// según el tipo (rol) de usuario.
// ---------------------------------
/*
function mytheme_custom_login_redirect($redirect_to, $request, $user) {
    global $user;
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {

        // Si es un usuario administrador
        // le redirigimos a la página de gestión de plugins
        if ( in_array( 'administrator', $user->roles ) ){
        	return home_url( '/wp-admin' );
					// wp_safe_redirect( '/wp-admin' );
					// exit;
        // Si es un usuario con permisos de editor
        // le enviamos a la página de gestión de entradas.
        //elseif ( in_array( 'editor', $user->roles ) )
        //    return home_url( '/wp-admin/edit.php' );

        // Y a todos los demás usuarios
        // les redirigimos a la página de inicio de la web.
			}else{
				// wp_safe_redirect( home_url() );
				// exit;
        return home_url();
			}

    } else {
			// wp_safe_redirect( $redirect_to );
			// exit;
      return $redirect_to;
    }
}
add_filter( 'login_redirect', 'mytheme_custom_login_redirect', 10, 3 );
//add_action( 'admin_init', 'mytheme_custom_login_redirect', 10, 3 );
*/

/*
if ( (isset($_GET['action']) && $_GET['action'] != 'logout') || (isset($_POST['login_location']) && !empty($_POST['login_location'])) ) {
	add_filter('login_redirect', 'my_login_redirect', 10, 3);
		function my_login_redirect() {
				$location = $_SERVER['HTTP_REFERER'];
				echo 'test:';
				echo $location;
				wp_safe_redirect($location);
				exit();
	}
}
*/

/*
function redireccion_despues_login(){
	if (ICL_LANGUAGE_CODE == 'es'){
		//$redirect_to = '/'.ICL_LANGUAGE_CODE.'/';
		wp_redirect( '/'.ICL_LANGUAGE_CODE.'/' );
	}
	if (ICL_LANGUAGE_CODE == 'en'){
		//$redirect_to = '/';
		wp_redirect( '/' );
	}
	//wp_redirect( $redirect_to );
	exit();
}
add_filter( 'login_redirect', 'redireccion_despues_login', 10, 3 );

function redireccion_despues_logout(){
		if (ICL_LANGUAGE_CODE == 'es'){
	 		//$redirect_to = '/'.ICL_LANGUAGE_CODE.'/';
			wp_redirect( '/'.ICL_LANGUAGE_CODE.'/' );
	 	}
		if (ICL_LANGUAGE_CODE == 'en'){
		 	//$redirect_to = '/';
			wp_redirect( '/' );
		}
		//wp_redirect( $redirect_to );
		exit();
}
add_action('wp_logout','redireccion_despues_logout');
*/

// function my_custom_login_redirect(){
// 	//$idioma = ICL_LANGUAGE_CODE;
// 	$idioma = ICL_LANGUAGE_CODE;
// 	echo 'LENGUAJE: '.$idioma.'<br>';
// 	if (ICL_LANGUAGE_CODE == 'es'){
// 		//wp_redirect( home_url('/es') );
//   	wp_redirect( home_url('/'.ICL_LANGUAGE_CODE.'/') );
// 	}
// 	if (ICL_LANGUAGE_CODE == 'en'){
//   	wp_redirect( home_url("/") );
// 	}
//   exit();
// }
// add_action( 'wp_login','my_custom_login_redirect' );
/**
* Redireccionar después de hacer logout
*/
// function my_custom_logout_redirect(){
// 	if (ICL_LANGUAGE_CODE == 'es'){
//   	//wp_redirect( home_url("/es") );
// 		wp_redirect( home_url('/'.ICL_LANGUAGE_CODE.'/') );
// 	}
// 	if (ICL_LANGUAGE_CODE == 'en'){
//   	wp_redirect( home_url("/") );
// 	}
//   exit();
// }
// add_action( 'wp_logout', 'my_custom_logout_redirect' );


add_action( 'show_user_profile', 'agregar_campos_seccion' );
add_action( 'edit_user_profile', 'agregar_campos_seccion' );

function agregar_campos_seccion( $user ) {
?>
    <h3><?php _e('Bio Español'); ?></h3>

    <table class="form-table">
        <tr>
            <th>
                <label for="profesion"><?php _e('Bio Es'); ?></label>
            </th>
            <td>
                <input type="text" name="bioes" id="bioes" class="regular-text"
                	value="<?php echo esc_attr( get_the_author_meta( 'bioes', $user->ID ) ); ?>" />
                <p class="description"><?php _e('Ingresa tu bio'); ?></p>
            </td>
        </tr>
    </table>
<?php }

//Guardamos los nuevos campos
add_action( 'personal_options_update', 'grabar_campos_seccion' );
add_action( 'edit_user_profile_update', 'grabar_campos_seccion' );

function grabar_campos_seccion( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) ) {
        return false;
    }

    if( isset($_POST['bioes']) ) {
        $profesion = $_POST['bioes'];
        update_user_meta( $user_id, 'bioes', $profesion );
    }
}
