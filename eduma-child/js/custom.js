jQuery(document).ready(function($) {

  if ($('.wpml-ls-current-language').length > 0){
    $('.wpml-ls-current-language').addClass('idioma-actual');
  }

  if ($('.course-description').length > 0){
    if ($('.thim-course-content').length > 0){
      $('.thim-course-content').addClass('caja1');
    }
    if ($('.thim-course-content').length > 0){
      $('.thim-course-info').addClass('caja2');
      $('.thim-course-info ul').addClass('col3');
    }
  }

  $('.titulo_faq_js').click(function(){
    if ( $(this).hasClass('plegado') ){
      let id = this.id;
      //alert(id);
      $(this).removeClass('plegado');
      $(this).addClass('desplegado');
      //$('.faq_contenido').css('display', 'block');
      $('.'+id).css('display', 'block');
    }else{
      let id = this.id;
      $(this).removeClass('desplegado');
      $(this).addClass('plegado');
      //$('.faq_contenido').css('display', 'none');
      $('.'+id).css('display', 'none');
    }
    /*
    if ( $(this).hasClass('plegado') ){
      $(this).removeClass('plegado');
      $(this).addClass('desplegado');
      $('.faq_plegado').css('display', 'block');
    }else{
      $(this).removeClass('desplegado');
      $(this).addClass('plegado');
      $('.faq_plegado').css('display', 'none');
    }
    */
  });

  var pathname = window.location.pathname;
  //alert(pathname);
  /* Si entramos en la parte de la visualización de módulos */
  if (pathname.includes('lessons')){
    //alert('pathname include lessons');
    /* Modificaciones a style en función de la carga de las lessons */
    $('#learn-press-content-item').addClass('leccionview_content');
    $('#sidebar').addClass('leccionhide');
    $('#course-curriculum-popup').addClass('leccionview_curriculum');
    $('.footer').addClass('leccionview_footer');
    /* Modificaciones a style en función de la visualización de la barra de administrador */
    if ($('#main-content').length > 0){
      $('#main-content').addClass('lessons_admin_bar');
      $('#wpadminbar').addClass('viewadminbar_admin');
      $('#masthead').addClass('viewadminbar_menu');
      $('.row').addClass('viewadminbar_fila');
      $('.no-sticky-logo').addClass('viewadminbar_logo');
    }
  }

  /* ocultar barra lateral en buscador de cursos */
  if (pathname.includes('/es/cursos/') || pathname.includes('/courses/')){
    //alert(pathname);
    $('.course-price').addClass('invisible-academy');
    $('.boton_comprar_barra').addClass('invisible-academy');
    $('.rating-box').addClass('invisible-academy');
    $('.tu_progreso_barra').addClass('invisible-academy');
    $('.porcentaje_completado_barra').addClass('invisible-academy');
    //$('.boton_continuar_barra').addClass('invisible-academy');
  }

  /* Ocultar comentarios fuera de la lección */
  if (!pathname.includes('lessons')){
    if ( $('#comments') ) {
      $('#comments').addClass('oculta_comentarios_fuera_leccion');
    }
  }

  if (pathname.includes('faqs')){
    $('#sidebar').addClass('no-sidebar-faqs');
    $('.thim-about-author').addClass('no-autor-faqs');
    $('.entry-navigation-post').addClass('no-navegacion-faqs');
    $('.entry-tag-share').addClass('no-entry-faqs');
  }

  /* Ocultar flechas con primera y última lección */
  if ($('.course-item-prev').length == 0){
    $('.course-item-nav').addClass('no-before');
    $('.course-item-next').addClass('sin-before');
    $('.course-item-nav').addClass('recalcular-after');
  }
  if ($('.course-item-next').length == 0){
    $('.course-item-nav').addClass('no-after');
  }

  /* Cambiar chevrones al plegar y desplegar lección */
  if ($('.section-header').length > 0){
    // Al hacer click en un módulo, cambiar class y chevron
    $('.section-header').click(function(){
      if ( $(this).hasClass('abierto') ){
        $(this).removeClass('abierto');
        $(this).addClass('cerrado');
      }else{
        $(this).removeClass('cerrado');
        $(this).addClass('abierto');
      }
    });

    // recorrer li del ul al mismo nivel que h4 para ver si tiene la clase current, si no, cerrado, si la tiene, abierto
    $('.section-header').next().each(function(){
      $(this).find('li').each(function(){
        var elemento = $(this);
        // comprobación de que el elemento contiene la clase current
        if ( elemento.hasClass('current') ){
          // elimino la clase cerrado, si existe en el elemento this (ul)
          $(this).parent().removeClass('cerrado');
          // agrego la clase abierto al elemento this (ul)
          $(this).parent().addClass('abierto');

          // elimino la clase cerrado del elemento padre y anterior (h4)
          $(this).parent().prev().removeClass('cerrado');
          // agrego la clase abierto al elemento padre y anterior (h4)
          $(this).parent().prev().addClass('abierto');
          return false;
        }
        // Comprobación de que el elemento no contiene la clase current
        if ( !elemento.hasClass('current') ){
          // agrego al elemento this (ul) la clase cerrado
          $(this).parent().addClass('cerrado');
        }
      });
    });
  }
});
